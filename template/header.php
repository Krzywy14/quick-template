<?php
/**
 *
 * @package Adream
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--[if lt IE 10]>
	<div class="alert alert-warning">
		You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
	</div>
<![endif]-->
	<?php // get_template_part('view/parts/image-extender'); ?>
	<?php // get_template_part('view/parts/cookies'); ?>
	<!--<div id="psd"></div>
	<div id="psd_switch">PSD ON/OFF</div> -->
	<!-- <div id="go_to_top"></div> -->
<div class="wrapper">
	<div class="parent header parent--nav">
		<section class="row container row--nav">
			<?php  get_template_part('view/parts/nav'); ?>
		</section>
	</div>
	<header class="parent parent--header header">
		<div class="header__mask">
			<section class="row container">
				<div class="col col--1">
					<h1 class="border border--bottom">Lorem ipsum</h1>
					<h2>Lorem ipsum dolor sit</h2>
				</div>
			</section>
		</div>
	</header>