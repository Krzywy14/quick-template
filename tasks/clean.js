'use strict'

import gulp			from "gulp";
import del			from "del"

import config		from './config';

export default () => {
	return del.sync(config.clean_pattern, {force:true});
}
